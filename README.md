Configuration
============

```yaml

// config/packages/security.yaml

providers:
    users:
        entity:
               
           class: 'App\Entity\User'
               
           property: 'username'    
    users_by_token:
        entity:
               
           class: 'App\Entity\User'
               
           property: 'token'

token:
    provider: users_by_token
    guard:
       authenticators:
          - MiCore\ApiSecurityBundle\Security\TokenAuthenticator
    access_denied_handler: MiCore\ApiSecurityBundle\Security\ApiAccessDeniedHandler
    stateless: true

user_name:
    provider: users
    guard:
       authenticators:
          - MiCore\ApiSecurityBundle\Security\LoginAuthenticator   
    stateless: true
```
