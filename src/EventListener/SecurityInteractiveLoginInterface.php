<?php


namespace MiCore\ApiSecurityBundle\EventListener;


use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

interface SecurityInteractiveLoginInterface
{

    /**
     * @param InteractiveLoginEvent $event
     * @return mixed
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event);

}
