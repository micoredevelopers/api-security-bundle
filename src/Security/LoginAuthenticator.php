<?php

namespace MiCore\ApiSecurityBundle\Security;

use MiCore\ApiBundle\Api\ApiService;
use MiCore\ApiSecurityBundle\Security\LoginManager\LoginManagerInterface;
use MiCore\ApiSecurityBundle\Security\LoginManager\UserLoginCredentials;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class LoginAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var LoginManagerInterface
     */
    private $loginManager;

    /**
     * @var ApiService
     */
    private $apiService;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        UserPasswordEncoderInterface $passwordEncoder,
        LoginManagerInterface $loginManager,
        ApiService $apiService
    )
    {
        $this->urlGenerator = $urlGenerator;
        $this->passwordEncoder = $passwordEncoder;
        $this->loginManager = $loginManager;
        $this->apiService = $apiService;
    }

    public function supports(Request $request)
    {
        return $this->loginManager->validateRequest($request);
    }

    /**
     * @param Request $request
     * @return mixed|UserLoginCredentials
     */
    public function getCredentials(Request $request)
    {
        return $this->loginManager->getCredentials($request);
    }

    /**
     * @param UserLoginCredentials $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!$credentials->getUsername()) {
            return null;
        }

//        if (!is_string($credentials->getUsername()) || !trim($credentials->getUsername())) {
//            throw new CustomUserMessageAuthenticationException('Email could not be found.');
//        }
        return $userProvider->loadUserByUsername($credentials->getUsername());
    }

    /**
     * @param UserLoginCredentials $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        if (null === $credentials->getPassword()){
            return false;
        }
        return $this->passwordEncoder->isPasswordValid($user, $credentials->getPassword());
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $user = $token->getUser();
        return $this
            ->apiService
            ->createResponseBuilder()
            ->setData($user)
            ->setNormalizerGroups(['safe', 'auth'])
            ->createResponse();
    }


    public function start(Request $request, AuthenticationException $authException = null)
    {
        return $this->_onAuthenticationFailure($request);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return $this->_onAuthenticationFailure($request);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe(): bool
    {
        return false;
    }

    private function _onAuthenticationFailure(Request $request)
    {

        $msg = 'Login or password is entered incorrectly!';

        return $this->apiService->createResponseBuilder()
            ->setStatus(401)
            ->setMsg($msg)
            ->addError($msg)
            ->createResponse();
    }
}
