<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 11.05.19
 * Time: 17:20
 */

namespace MiCore\ApiSecurityBundle\Security;


use MiCore\ApiBundle\Api\ApiService;
use MiCore\ApiBundle\Api\Error\ErrorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class ApiAccessDeniedHandler implements AccessDeniedHandlerInterface
{

    /**
     * @var ApiService
     */
    private $apiResponseService;

    public function __construct(ApiService $apiResponseService)
    {
        $this->apiResponseService = $apiResponseService;
    }

    /**
     * Handles an access denied failure.
     *
     * @param Request $request
     * @param AccessDeniedException $accessDeniedException
     * @return Response
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {

        $responseBuilder = $this->apiResponseService->createResponseBuilder();
        $responseBuilder
            ->setStatus(403)
            ->addError('Access to the resource is denied!', ErrorInterface::ERROR_PROPERTY_PATH_ROOT)
            ->setMsg('Access to the resource is denied!');

        return $responseBuilder->createResponse();
    }
}
