<?php

namespace MiCore\ApiSecurityBundle\Security\LoginManager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class LoginManager implements LoginManagerInterface
{

    /**
     * @var string
     */
    private $routeName;
    /**
     * @var string
     */
    private $usernameField;
    /**
     * @var string
     */
    private $passwordField;

    public function __construct(string $routeName = 'app_api_login', string $usernameField = 'username', $passwordField = 'password')
    {
        $this->routeName = $routeName;
        $this->usernameField = $usernameField;
        $this->passwordField = $passwordField;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function validateRequest(Request $request): bool
    {
        return $this->routeName === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    /**
     * @param Request $request
     * @return UserLoginCredentials
     */
    public function getCredentials(Request $request): UserLoginCredentials
    {
        if ($request->getContentType() === 'json' && $request->getContent()) {
            try {
                $data = json_decode($request->getContent(), true);
                $request->request->replace(is_array($data) ? $data : array());
            } catch (\Exception $exception){
                throw new BadRequestHttpException('invalid json body: ' . json_last_error_msg());
            }
        }

        $username = $request->request->get($this->usernameField);
        $password = $request->request->get($this->passwordField);

        return new UserLoginCredentials($username, $password);
    }
}
