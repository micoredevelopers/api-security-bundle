<?php

namespace MiCore\ApiSecurityBundle\Security\LoginManager;

class UserLoginCredentials
{

    /**
     * @var string|null
     */
    private $username;

    /**
     * @var string|null
     */
    private $password;

    public function __construct($username, $password)
    {

        if (is_scalar($username)) {
            $username = strtolower((string)$username);
            $this->username = trim($username) ?? null;
        }

        if (is_scalar($password)){
            $this->password = trim((string)$password) ?? null;
        }

    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

}
