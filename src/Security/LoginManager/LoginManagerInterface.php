<?php

namespace MiCore\ApiSecurityBundle\Security\LoginManager;

use Symfony\Component\HttpFoundation\Request;

interface LoginManagerInterface
{

    /**
     * @param Request $request
     * @return bool
     */
    public function validateRequest(Request $request): bool;

    /**
     * @param Request $request
     * @return UserLoginCredentials
     */
    public function getCredentials(Request $request): UserLoginCredentials;

}
