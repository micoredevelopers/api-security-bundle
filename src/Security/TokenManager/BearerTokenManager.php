<?php


namespace MiCore\ApiSecurityBundle\Security\TokenManager;


use Symfony\Component\HttpFoundation\Request;

class BearerTokenManager implements TokenManagerInterface
{

    /**
     * @param Request $request
     * @return bool
     */
    public function validateRequest(Request $request): bool
    {
        return null !== $this->_getToken($request);
    }

    /**
     * @param Request $request
     * @return string|null
     */
    public function getToken(Request $request): ?string
    {
       return $this->_getToken($request);
    }

    /**
     * @param Request $request
     * @return string|null
     */
    protected function _getToken(Request $request): ?string
    {
        if ($request->headers->has('Authorization')){
            $token = $request->headers->get('Authorization');
            if (null !== $token && '' !== $token) {
                return substr($token, 7);
            }
        }
        return null;
    }
}
