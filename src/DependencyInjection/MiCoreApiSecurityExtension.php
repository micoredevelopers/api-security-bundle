<?php


namespace MiCore\ApiSecurityBundle\DependencyInjection;


use MiCore\ApiSecurityBundle\EventListener\SecurityInteractiveLoginInterface;
use MiCore\ApiSecurityBundle\Security\ApiAccessDeniedHandler;
use MiCore\ApiSecurityBundle\Security\LoginAuthenticator;
use MiCore\ApiSecurityBundle\Security\LoginManager\LoginManager;
use MiCore\ApiSecurityBundle\Security\LoginManager\LoginManagerInterface;
use MiCore\ApiSecurityBundle\Security\TokenAuthenticator;
use MiCore\ApiSecurityBundle\Security\TokenManager\BearerTokenManager;
use MiCore\ApiSecurityBundle\Security\TokenManager\TokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\Security\Http\SecurityEvents;

class MiCoreApiSecurityExtension extends Extension
{


    public function load(array $configs, ContainerBuilder $container)
    {

        $container->autowire(TokenManagerInterface::class, BearerTokenManager::class);
        $container->autowire(LoginManagerInterface::class, LoginManager::class);
        $container->autowire(LoginAuthenticator::class);
        $container->autowire(TokenAuthenticator::class);
        $container->autowire(ApiAccessDeniedHandler::class);
        $container
            ->registerForAutoconfiguration(SecurityInteractiveLoginInterface::class)
            ->addTag('kernel.event_listener', [
                'event' => SecurityEvents::INTERACTIVE_LOGIN,
                'method' => 'onInteractiveLogin'
            ]);
        if (isset($_ENV['APP_ENV']) && 'test' == $_ENV['APP_ENV']){

            foreach ($container->getDefinitions() as $id=>$definition){
                $definition->setPublic(true);
            }
        }

    }
}
