<?php


namespace MiCore\ApiSecurityBundle\Tests\Fixtures;


use MiCore\ApiSecurityBundle\MiCoreApiSecurityBundle;
use MiCore\KernelTest\KernelTest;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Kernel extends KernelTest
{

    public function configureContainer(ContainerBuilder $c)
    {
        $c->loadFromExtension('security', [

        ]);
    }

    protected function bundles(): array
    {
        return [
            MiCoreApiSecurityBundle::class,
            SecurityBundle::class
        ];
    }
}
