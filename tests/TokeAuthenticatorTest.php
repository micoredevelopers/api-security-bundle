<?php


namespace MiCore\ApiSecurityBundle\Tests;


use MiCore\ApiSecurityBundle\Security\TokenAuthenticator;
use MiCore\KernelTest\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TokeAuthenticatorTest extends KernelTestCase
{

    private function getTokenAuthenticator(): TokenAuthenticator
    {
        return self::$container->get(TokenAuthenticator::class);
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|UserProviderInterface
     */
    private function getUserProviderMock()
    {
        $mock = $this->createMock(UserProviderInterface::class);
        $mock->method('loadUserByUsername')->willReturnCallback(function($userName){
            if ('test_token' === $userName){
                $userMock =  $this->createMock(UserInterface::class);
                $userMock->method('getUsername')->willReturn('test_token');
                return $userMock;
            }
            return null;
        });
        return $mock;
    }

    public function testUserProviderMock()
    {
        $userProvider = $this->getUserProviderMock();
        $user1 = $userProvider->loadUserByUsername('test_token');
        $user2 = $userProvider->loadUserByUsername('foo');

        $this->assertEquals($user1->getUsername(), 'test_token');
        $this->assertNull($user2);
    }

    /**
     * @param array $headers
     * @param $token
     *
     * @dataProvider dataProviderTokenAuthenticator
     */
    public function testTokenAuthenticator(array $headers, $token)
    {
        $authenticator = $this->getTokenAuthenticator();
        $provider = $this->getUserProviderMock();

        $request = new Request();
        foreach ($headers as $k => $v){
            $request->headers->set($k, $v);
        }

        $supportAssert = $token !== null;

        $support = $authenticator->supports($request);
        $cred = $authenticator->getCredentials($request);
        $user = $authenticator->getUser($cred, $provider);

        $this->assertEquals($supportAssert, $support);
        $this->assertEquals($token, $user ? $user->getUsername() : null);

    }

    public function dataProviderTokenAuthenticator(): array
    {
        return [
            [['Authorization'=> 'Bearer test_token'], 'test_token']
        ];
    }



}
